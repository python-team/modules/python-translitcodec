Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: translitcodec
Source: http://pypi.python.org/pypi/translitcodec

Files: *
Copyright: 2008 the translitcodec authors and developers, see AUTHORS.
 Copyright (c) 2008 Jason Kirtland <jek at discorporate us>
 Craig Dennis <craig@idealist.org>
License: Expat
 Copyright (c) 2008 Jason Kirtland <jek at discorporate us>
 .
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Files: transtab/*
Copyright: 2000, Markus Kuhn <Markus.Kuhn@cl.cam.ac.uk>
Comment: This code is, as the author describes, an unfinished work, and
 so the copyright attribution is somewhat loose. However the author
 has asserted fairly strongly in the email in the License section that
 the license can be chosen from a broad list, and since the rest of the
 project is MIT/Expat, this seems to be the logical choice.
 .
 The Copyright date is taken from the modified times in
 http://www.cl.cam.ac.uk/~mgk25/download/transtab.tar.gz
License: Expat
 The following email message from the original author asserts the license
 for the code:
 .
 To: Clint Byrum <clint@ubuntu.com>
 cc: Jason Kirtland <jek@discorporate.us>
 Subject: Re: Copyright and License status of "transtab" 
 In-reply-to: Your message of "Sun, 29 Jan 2012 23:34:06 PST."
              <1327908570-sup-2854@fewbar.com> 
 X-URL: http://www.cl.cam.ac.uk/~mgk25/
 X-image-url: http://www.cl.cam.ac.uk/~mgk25/markus2-48.jpg
 Mime-Version: 1.0
 Content-Type: text/plain; charset=us-ascii
 Date: Mon, 30 Jan 2012 11:23:38 +0000
 From: Markus Kuhn <Markus.Kuhn@cl.cam.ac.uk>
 Message-Id: <E1RrpL2-0000pr-Ft@mta1.cl.cam.ac.uk>
 . 
 Clint Byrum wrote on 2012-01-30 07:34 UTC:
 > My name is Clint, and I'm working on Packaging the python library
 > 'translitcodec' for Debian.  In the course of packaging and review, its
 > become clear that the license status of one directory of the library
 > is ambiguous. Because Debian follows strict guidelines to both avoid
 > liability for its developers, and also safeguard user freedom, the
 > software won't be accepted without first clarifying this.
 > 
 > The software in question is available here:
 > 
 > http://pypi.python.org/pypi/translitcodec/
 > 
 > Jason, you are the attributed "author". It claims to make use of the
 > transtab data provided by you, Dr. Markus Kuhn.
 > 
 > Can either of you provide documentation of the copyright status and/or
 > license granted to this data?
 .
 http://www.cl.cam.ac.uk/~mgk25/download/transtab.tar.gz
 .
 Should I ever get around to making another transtab release (an old,
 unfinished project), I'll try to remember to add the following license
 text somewhere:
 .
   Markus Kuhn <http://www.cl.cam.ac.uk/~mgk25/> -- yyyy-mm-dd
   http://www.cl.cam.ac.uk/~mgk25/short-license.html
 .
 Executive summary: I promise not to sue you.
 .
 Hope this helped ...
 .
 Markus
 .
 -- 
 Markus Kuhn, Computer Laboratory, University of Cambridge
 http://www.cl.cam.ac.uk/~mgk25/ || CB3 0FD, Great Britain
 .
 The following is the text of the sugested url, retrieved on Sunday,
 February 5, around 22:28 UTC
 .
 http://www.cl.cam.ac.uk/~mgk25/short-license.html
 Short code license
 .
 If you have reached this web page because you found its URL included as a
 license-reference comment in a short piece of published computer software
 source code by its author(s), next to their name, as in, for example,
 .
   Markus Kuhn <http://www.cl.cam.ac.uk/~mgk25/> -- 1999-12-31
   License: http://www.cl.cam.ac.uk/~mgk25/short-license.html
 .
 then you can assume the following:
 .
 This code was published by its author(s) as an easily reusable piece of free
 software, for example to demonstrate some particular programming practice. It
 is likely too short or obvious to skilled programmers to fall under, or
 deserve, the protection of copyright legislation. It is also likely to be
 useless on its own, unless verified and integrated into a larger program by an
 experienced software engineer, and therefore no consumer protection, warranty
 or liability rights apply either. Therefore, its author(s) refused to disfigure
 its appearance with a lengthy copyright license text.
 .
 Nevertheless, overly cautious lawyers (possibly as part of a “due diligence
 exercise”) occasionally contact authors of such short code snippets for a
 formal copyright license.
 .
 Therefore, the author(s) agree(s) to clarify that, at the user’s choice, the
 code can be used under any of the following licenses, or any compatible with
 them:
 .
   • Apache License, 2.0
   • BSD license
   • GNU General Public License (GPL)
   • GNU Library or "Lesser" General Public License (LGPL)
   • MIT license
   • Mozilla Public License 1.1 (MPL)
   • Common Development and Distribution License
   • Eclipse Public License
 .
 Markus Kuhn
 .
 created 2011-02-15 – last modified 2011-02-15 – http://www.cl.cam.ac.uk/~mgk25/

Files: debian/*
Copyright: 2012 Canonical Ltd. All Rights Reserved.
License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
